sqlite-utils (3.38-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 27 Nov 2024 16:32:59 +0000

sqlite-utils (3.38~a0-1) unstable; urgency=medium

  * New upstream release.
  * Adjust debian/watch to handle alpha and beta versions.
  * Move packages required for tests and docs to Build-Depends-Indep.
  * Update Standards-Version.
  * Add pybuild-plugin-pyproject to Build-Depends.
  * Drop 0002-less-requirements-for-documentation.patch and add
    python3-beanbag-docutils Build-Depends.

 -- Edward Betts <edward@4angle.com>  Sun, 17 Nov 2024 10:56:33 +0100

sqlite-utils (3.36-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.

 -- Edward Betts <edward@4angle.com>  Thu, 07 Mar 2024 11:58:06 +0100

sqlite-utils (3.35.2-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 08 Nov 2023 14:09:21 +0100

sqlite-utils (3.35.1-1) unstable; urgency=medium

  * New upstream release.
  * Update 0001-fix-documentation-privacy-breach.patch to remove plausible.io
    script from documentation.
  * Documentation builds successfully with Sphinx 7.1 and docutils 0.20.
    (Closes: #1042705)
  * Add !nocheck to Build-Depends that are only needed for testing.

 -- Edward Betts <edward@4angle.com>  Mon, 30 Oct 2023 08:53:50 +0000

sqlite-utils (3.34-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.
  * Update Standards-Version.
  * Refresh patches for new upstream.
  * Drop the patch that switched sphinx documentation themes because the furo
    theme is now available in Debian.
  * Add Build-Depends for furo.

 -- Edward Betts <edward@4angle.com>  Mon, 07 Aug 2023 09:09:26 +0100

sqlite-utils (3.30-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Tue, 01 Nov 2022 15:02:33 +0000

sqlite-utils (3.29-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version.
  * Update documentation privacy patch.
  * Upstream switched to the furo sphinx theme which is not in Debian and is
    difficult to package, so patch the documentation to use sphinx_rtd_theme.
  * Patch the documentation to not use github_linkcode_resolve because it
    broke the build.
  * Fix broken watch file.

 -- Edward Betts <edward@4angle.com>  Sat, 01 Oct 2022 15:51:05 +0100

sqlite-utils (3.26.1-1) unstable; urgency=medium

  * New upstream release.
  * Add python3-click-default-group to Depends in debian/tests/control.

 -- Edward Betts <edward@4angle.com>  Thu, 26 May 2022 19:18:33 +0200

sqlite-utils (3.25-2) unstable; urgency=medium

  * Team upload.
  * Move ${sphinxdoc:Built-Using} to the correct field.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 03 Apr 2022 14:22:14 +0300

sqlite-utils (3.25-1) unstable; urgency=medium

  * New upstream release.
  * Workaround reproducibility problem by setting LANGUAGE=C.UTF-8 when
    building sphinx documentation.

 -- Edward Betts <edward@4angle.com>  Thu, 10 Mar 2022 10:55:18 +0100

sqlite-utils (3.24-2) unstable; urgency=medium

  * Source-only upload to allow package to migrate to testing.
  * Remove unnecessary package prefix from files in debian dir.

 -- Edward Betts <edward@4angle.com>  Sat, 26 Feb 2022 07:44:47 +0000

sqlite-utils (3.24-1) unstable; urgency=medium

  * Initial release. (Closes: #1006328)

 -- Edward Betts <edward@4angle.com>  Thu, 24 Feb 2022 21:36:04 +0000
